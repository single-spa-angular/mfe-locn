import { Component } from '@angular/core';

@Component({
  selector: 'locn-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'locn';
}
